import React, { Component } from 'react'
import { Dropdown } from 'semantic-ui-react'

class VeiculosDropdown extends Component {
    
  render(){
    return (
        <Dropdown text='Veiculos'>
            <Dropdown.Menu>
            <Dropdown.Item text='Nome' />
            <Dropdown.Item text='HTTP'/>
            <Dropdown.Item text='Tiragem' />
            <Dropdown.Item text='Cidade'  />
            <Dropdown.Item text='UF' />
            <Dropdown.Item icon='ESTADO' />
            <Dropdown.Item icon='Tipo de Veiculo' />
            <Dropdown.Item text='Regiao' />
            </Dropdown.Menu>
        </Dropdown>
    );
  }
}

export default VeiculosDropdown;