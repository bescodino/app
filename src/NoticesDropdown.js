import React, { Component } from 'react'
import { Dropdown } from 'semantic-ui-react'

class NoticesDropdown extends Component {
    
  render(){
    return (
        <Dropdown text='Noticias'>
            <Dropdown.Menu>
            <Dropdown.Item text='Veiuclos' />
            <Dropdown.Item text='URL' description='ctrl + o' />
            <Dropdown.Item text='Titulo' description='ctrl + s' />
            <Dropdown.Item text='Texto' description='ctrl + r' />
            <Dropdown.Item text='Data Captura' />
            <Dropdown.Item icon='Print' text='Move to folder' />
            <Dropdown.Item icon='Categoria' text='Move to trash' />
            <Dropdown.Item text='Data Completa' />
            <Dropdown.Item text='Autor' />
            <Dropdown.Item text='Analise' />
            </Dropdown.Menu>
        </Dropdown>
    );
  }
}

export default NoticesDropdown;