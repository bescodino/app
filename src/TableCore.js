import React, { Component }  from 'react'
import { Table } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css'
import _ from 'lodash';

class TableCore extends Component {

    render() {
        const { core, data } = this.props;
        console.log(data)

        return <Table >
                <Table.Header>
                   {
                    core === 'noticias' ?
                        <Table.Row>
                            <Table.HeaderCell>Veiculo</Table.HeaderCell>
                            <Table.HeaderCell>Url</Table.HeaderCell>
                            <Table.HeaderCell>Titulo</Table.HeaderCell>
                            <Table.HeaderCell>Texto</Table.HeaderCell>
                            <Table.HeaderCell>Autor</Table.HeaderCell>
                            <Table.HeaderCell>Data Captura</Table.HeaderCell>
                            <Table.HeaderCell>Print</Table.HeaderCell>
                            <Table.HeaderCell>Categoria</Table.HeaderCell>
                            <Table.HeaderCell>Data Completa</Table.HeaderCell>
                            <Table.HeaderCell>Analise</Table.HeaderCell>
                        </Table.Row> 
                        :
                        <Table.Row>
                            <Table.HeaderCell>Nome</Table.HeaderCell>
                            <Table.HeaderCell>URL</Table.HeaderCell>
                            <Table.HeaderCell>Tiragem</Table.HeaderCell>
                            <Table.HeaderCell>CIDADE</Table.HeaderCell>
                            <Table.HeaderCell>UF</Table.HeaderCell>
                            <Table.HeaderCell>Estado</Table.HeaderCell>
                            <Table.HeaderCell>Tipo de Veiculo</Table.HeaderCell>
                            <Table.HeaderCell>Região</Table.HeaderCell>
                        </Table.Row>
                   }
                </Table.Header>
            
                <Table.Body>
                    {
                        core === 'veiculos' ?  data.map(item => 
                        (
                            <Table.Row>
                                <Table.Cell>{item.Nome}</Table.Cell>
                                <Table.Cell>{item.HTTP}</Table.Cell>
                                <Table.Cell>{item.Tiragem}</Table.Cell>
                                <Table.Cell>{item.CIDADE}</Table.Cell>
                                <Table.Cell>{item.UF}</Table.Cell>
                                <Table.Cell>{item.ESTADO}</Table.Cell>
                                <Table.Cell>{item.Tipo_de_Ve_culo}</Table.Cell>
                                <Table.Cell>{item.Regi_o}</Table.Cell>
                            </Table.Row>
                        ))
                        
                        :
                        data.map(item => 
                        (
                            <Table.Row>
                                <Table.Cell>{item.veiculo}</Table.Cell>
                                <Table.Cell>{item.url}</Table.Cell>
                                <Table.Cell>{item.titulo}</Table.Cell>
                                <Table.Cell>{item.texto}</Table.Cell>
                                <Table.Cell>{item.autor}</Table.Cell>
                                <Table.Cell>{item.data_captura}</Table.Cell>
                                <Table.Cell>{item.print}</Table.Cell>
                                <Table.Cell>{item.categoria}</Table.Cell>
                                <Table.Cell>{item.data_completa}</Table.Cell>
                                <Table.Cell>{item.analise}</Table.Cell>
                            </Table.Row>
                        )) 
                    }
                </Table.Body>

                {/* <Table.Footer>
                <Table.Row>
                    <Table.HeaderCell colSpan='3'>
                    <Menu floated='right' pagination>
                        <Menu.Item as='a' icon>
                        <Icon name='chevron left' />
                        </Menu.Item>
                        <Menu.Item as='a'>1</Menu.Item>
                        <Menu.Item as='a'>2</Menu.Item>
                        <Menu.Item as='a'>3</Menu.Item>
                        <Menu.Item as='a'>4</Menu.Item>
                        <Menu.Item as='a' icon>
                        <Icon name='chevron right' />
                        </Menu.Item>
                    </Menu>
                    </Table.HeaderCell>
                </Table.Row>
                </Table.Footer> */}
            </Table>
    }
}

export default TableCore;