import React, {Component} from 'react';
import { Dropdown, Input } from 'semantic-ui-react'
import TableCore from './TableCore.js'
import 'semantic-ui-css/semantic.min.css';
import './App.css';

class App extends Component {

  constructor() {
    super();
    this.state = {
      cores: [
        { key: 'veiculos', value: 'veiculos', text: 'Veiculos' },
        { key: 'noticias', value: 'noticias', text: 'Noticias' }
      ],
      notices_columns: [
        { key: 'veiculo', value: 'veiculo', text: 'Veiculo' },
        { key: 'url', value: 'url', text: 'URL' },
        { key: 'titulo', value: 'titulo', text: 'Titulo' },
        { key: 'texto', value: 'texto', text: 'Texto' },
        { key: 'data_captura', value: 'data_captura', text: 'Data Captura' },
        { key: 'print', value: 'print', text: 'Print' },
        { key: 'categoria', value: 'categoria', text: 'categoria' },
        { key: 'data_completa', value: 'data_completa', text: 'Data Completa' },
        { key: 'analise', value: 'analise', text: 'Analise' },
        { key: 'autor', value: 'autor', text: 'Autor' },
      ],
      veiculos_columns: [
        { key: 'nome', value: 'nome', text: 'Nome' },
        { key: 'HTTP', value: 'HTTP', text: 'URL' },
        { key: 'Tiragem', value: 'Tiragem', text: 'Tiragem' },
        { key: 'CIDADE', value: 'CIDADE', text: 'Cidade' },
        { key: 'UF', value: 'UF', text: 'UF' },
        { key: 'ESTADO', value: 'ESTADO', text: 'Estado' },
        { key: 'Tipo_de_Ve_culo', value: 'Tipo_de_Ve_culo', text: 'Tipo de Veiculo' },
        { key: 'Regi_o', value: 'Regi_o', text: 'Região' }
      ],
      current_core: 'veiculos',
      current_columns: [
        { key: 'nome', value: 'nome', text: 'Nome' },
        { key: 'HTTP', value: 'HTTP', text: 'HTTP' },
        { key: 'Tiragem', value: 'Tiragem', text: 'Tiragem' },
        { key: 'CIDADE', value: 'CIDADE', text: 'Cidade' },
        { key: 'UF', value: 'UF', text: 'UF' },
        { key: 'ESTADO', value: 'ESTADO', text: 'Estado' },
        { key: 'Tipo_de_Ve_culo', value: 'Tipo_de_Ve_culo', text: 'Tipo de Veiculo' },
        { key: 'Regi_o', value: 'Regi_o', text: 'Região' }
      ],
      current_column: 'Nome',
      data: []
    };
    this.setState({current_columns: this.state.veiculos_columns});
  }

  handleColumn = (e,data) => {
    this.setState({
      current_column: data.value
    })
  }

  toggleShow = (e,data) => {
    
    this.setState({
      current_core: data.value,      
      current_columns: data.value === 'noticias' ? this.state.notices_columns : this.state.veiculos_columns,
    });
   };
1
   call_db = (e,data) => {

    this.setState({
      ...this.state,
      data: data.value ? this.state.data : []
    });
    
     if(data.value.toString())
     {
       console.log(this.state.current_column)
       console.log("http://servermon.ddns.net:8983/solr/" + this.state.current_core.toString() + "/select?q=" + this.state.current_column.toString() + ":" + data.value.toString() + '*'+ "&rows=60&wt=json")
      fetch("http://servermon.ddns.net:8983/solr/" + this.state.current_core.toString() + "/select?q=" + this.state.current_column.toString() + ":" + data.value.toString() + '*'+ "&rows=60&wt=json")
      .then(response => response.json())
      .then(res => {
        res.error ? res = ''
        :
        this.setState({
          ...this.state,
          data: res.response.docs
        })
        return res;
      });
     }
   }

  render() { return <header className="App-header">
     <Dropdown placeholder='Cores' onChange={this.toggleShow} options={this.state.cores} defaultValue='veiculos' />
     <br></br>
     <Dropdown options={this.state.current_columns} onChange={this.handleColumn} defaultValue={this.state.current_columns[0].value}/>
     <br></br>
     <Input placeholder='Search...' onChange={this.call_db}/>
     <br></br>
     <TableCore core={this.state.current_core} data={this.state.data} />
     
    </header>

  }
}

export default App;